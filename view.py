from flask import Flask

app = Flask(__name__)


@app.route('/')
def index():
    return "<h1> Home page </h1>"


@app.route('/admin')
def admin():
    return "<h1> Admin page</h1>"


if __name__ == "__main__":
    app.run()
